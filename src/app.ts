import { config as dotenv } from "dotenv";
import { PrismaClient } from "@prisma/client";
import { createServer as HTTPServer } from "http";
import { Server as SocketIO } from "socket.io";
import express from "express";
import Paper from "./PaperServer";

dotenv();
global.config = require("../config.json");

class App {
  readonly http = express();
  private readonly httpServer = HTTPServer(this.http);
  readonly socket = new SocketIO(this.httpServer);
  readonly database = new PrismaClient({
    datasources: { db: { url: process.env.DATABASE_URL } },
  });
  readonly paper = new Paper(process.env.PAPER_PATH!);
}

export default new App();
