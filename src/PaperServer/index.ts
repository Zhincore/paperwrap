import { spawn, ChildProcess } from "child_process";
import { randomBytes } from "crypto";
import EventEmitter from "events";
import Path from "path";
import fs from "fs";
import { Rcon } from "rcon-client";
import LogEvents from "./LogEvents";
import Properties from "./Properties";

const EXEC_NAME = "papermc";

export default class PaperServer extends EventEmitter {
  build?: number;
  version?: string;
  initPromise: Promise<void>;
  readonly properties: Properties;

  private process?: ChildProcess;
  readonly console: Rcon;

  constructor(readonly path: string) {
    super();
    this.initPromise = this.checkBuild().then(() => { /**/ });
    this.properties = new Properties(Path.join(this.path, "server.properties"));
    this.correctConfig();
    this.console = new Rcon({
      host: "localhost",
      port: +this.properties.get("rcon.port")!,
      password: this.properties.get("rcon.password") as string,
    });

    this.on("ready", async (sped: string) => {
      console.log(sped);
      this.properties.load();
    });

    this.on("rconReady", () => {
      this.console.connect();
      console.log("rcon ready");
    });
  }

  get isRunning() {
    return !!this.process;
  }

  get binaryName() {
    return `${EXEC_NAME}-${this.version}-${this.build}.jar`;
  }

  correctConfig() {
    const rconEnabled = this.properties.get("enable-rcon") === "true";
    const rconPassword = this.properties.get("rcon.password");

    // Enforce enabled rcon
    if (!rconEnabled) this.properties.set("enable-rcon", "true");
    // Enforce password
    if (!rconPassword || rconPassword.length < 16) this.properties.set("rcon.password", randomBytes(48).toString("base64"));
    this.properties.save();
  }

  async checkBuild() {
    const files = await fs.promises.readdir(this.path, { withFileTypes: true });

    let newestBuild: number|null = null;
    let newestVersion: string|null = null;

    for (const file of files) {
      if (!file.isFile || !file.name.startsWith(EXEC_NAME)) continue;
      const match = file.name.match(RegExp(EXEC_NAME+"-([0-9\\.]+)-([0-9]+)\\.jar"));
      if (!match) continue;

      if (!newestVersion || newestVersion < match[1]) newestVersion = match[1];
      if (!newestBuild || newestBuild < +match[2]) newestBuild = +match[2];
    }

    if (newestVersion) this.version = newestVersion;
    if (newestBuild) this.build = newestBuild;

    this.start();
    return {
      build: newestBuild,
      version: newestVersion,
    };
  }

  start() {
    if (this.process) throw new Error("Already started");

    this.process = spawn("java", [
      ...global.config.javaArgs,
      "-Xms"+process.env.MIN_RAM,
      "-Xmx"+process.env.MAX_RAM,
      "-jar", this.binaryName,
      "-nogui",
    ], {
      cwd: this.path,
    });

    // Log parsing
    const onLine = (line: string) => {
      const match = line.trim().match(/^\[\d\d:\d\d:\d\d (?<type>[A-Z]+)\]: (?<message>.+)$/);
      if (!match) return;
      const { type, message } = match.groups!;

      // Analyze log line
      if (type in LogEvents) {
        for (const [event, regex] of Object.entries(LogEvents[type])) {
          const eventMatch = message.match(regex);
          if (eventMatch) {
            this.emit(event, ...eventMatch.slice(1));
            break;
          }
        }
      }
    }

    let output = "";
    this.process.stdout!.on("data", (line: Buffer) => {
      output += line.toString();

      if (output.includes("\n")) {
        const lines = output.split("\n");
        output = lines.pop() || "";
        lines.forEach(onLine);
      }
    });
  }

  async stop() {
    if (!this.process) throw new Error("Not running");
    await this.console.end();
    return this.process.kill();
  }
}
