import fs from "fs";

export default class Properties extends Map<string, string> {
  header = "";

  constructor(readonly path: string) {
    super();
    this.load();
  }

  load() {
    this.clear();
    this.header = "";

    const file = fs.readFileSync(this.path, "utf-8");

    let doingHeader = true;
    for (let line of file.replace(/\\\n\s*/g, "").split(/\n/)) {
      line = line.trim();

      // Save header
      if (doingHeader) {
        if(!line.startsWith("#")) doingHeader = false;
        else {
          this.header += line + "\n";
          continue;
        }
      }

      const match = line.match(/^([a-z-.]+) ?(?:=|:) ?(.*)$/i) || [];
      if (!match || match.length < 3) continue;
      this.set(match[1], match[2]);
    }
  }

  save() {
    const conf = [...this.entries()]; //[...this.entries()].sort(); // Sorting is meaningless, paper resorts it again

    return fs.promises.writeFile(
      this.path,
      this.header + conf.map(line => line.join("=").replace(/\n/g, "\\n")).join("\n"),
    );
  }
}
