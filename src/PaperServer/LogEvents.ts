export default {
  INFO: {
    ready: /^Done \((\d+(?:\.\d+)?)s\)! For help, type "help"$/,
    rconReady: /^RCON running on (.+):(\d+)$/,
  },
} as Record<string, Record<string, RegExp>>;
