declare namespace NodeJS {
  interface Global {
    config: {
      paperAPI: string;
      javaArgs: string[];
    };
  }
}
